#!/bin/bash
if [ $(id -u) != 0 ]; then echo Must be run as root; exit 1; fi
killall -9 cthulhu rlyeh timingila celephais
killall -9 ubusd; sudo rm -f /tmp/*.log /var/run/ubus.sock
killall -9 rtrouted; rm -fr /tmp/rtroute*
umount -l /usr/share/cthulhu/rootfs/*
umount -l /lcm/cthulhu/rootfs/*
umount /lcm/cthulhu/data/images/generic
umount /lcm/cthulhu/data/mounts/generic
umount /usr/share/cthulhu/data/images/generic
umount /usr/share/cthulhu/data/mounts/generic
rm -rf /usr/share/rlyeh/*
rm -rf /usr/share/cthulhu/*
rm -rf /lcm/cthulhu/*
rm /usr/share/container_data/rlyeh_onboarded
rm /usr/share/rlyeh_onboarded
rm -rf /usr/share/rlyeh/*
rm -rf /var/lib/lxc/*
rm -rf /etc/config/*
rm -rf /usr/local/var/lib/lxc/*
