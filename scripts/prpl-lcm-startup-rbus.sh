#!/bin/bash
if [ $(id -u) != 0 ]; then echo Must be run as root; exit 1; fi
killall -9 cthulhu rlyeh timingila ubusd rtrouted
rm -f /tmp/*.log /var/run/ubus.sock
umount /usr/share/cthulhu/data/images/generic
sh -c 'echo 1 > /sys/fs/cgroup/memory/memory.use_hierarchy'
sh -c 'echo 1 > /sys/fs/cgroup/cpuset/cgroup.clone_children'
rtrouted -f -l DEBUG 2>&1 >/dev/null &
sleep 2
cthulhu -D
rlyeh -D
timingila -D
