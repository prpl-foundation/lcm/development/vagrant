# If not stated otherwise in this file or this component's LICENSE file the
# following copyright and licenses apply:
#
# Copyright 2022 Consult Red
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

=begin
################################################################################
Vagrantfile to configure a VirtualBox VM that can be used for LCM development.
################################################################################
=end

Vagrant.configure("2") do |config|

    ##################################################
    #
    # VM Configuration (Ubuntu 20.04)
    #
    config.vm.box = "ubuntu/focal64"

    config.vm.hostname = "lcm-vagrant-focal"
    config.vm.define "lcm-vagrant-focal"


    config.vm.network "public_network"

    config.vagrant.plugins = "vagrant-disksize"
    config.vagrant.plugins = "vagrant-vbguest"
    config.disksize.size = '25GB'

    if Vagrant.has_plugin?("vagrant-vbguest")
          config.vbguest.auto_update = false
    end

    config.vm.provider "virtualbox" do |vb|
        vb.memory = "4096"  # Change this to reduce the amount of RAM assigned to the VM
        vb.cpus = "6"       # Change this to reduce the amount of cores assigned to the VM
        vb.customize ["modifyvm", :id, "--ioapic", "on", "--vram", "100", "--graphicscontroller", "vmsvga", "--audio", "none"]
        vb.gui = false
    end

    ##################################################
    #
    # Git & SSH Config
    #
    # Forward SSH keys from host
    config.ssh.forward_agent = true

    # Copy host gitconfig
    config.vm.provision "file", source: "~/.gitconfig", destination: ".gitconfig"
    config.vm.provision "file", source: "~/.ssh", destination: "$HOME/.ssh"
    config.vm.provision "file", source: "./patches", destination: "$HOME/patches"
    config.vm.provision "file", source: "./scripts", destination: "$HOME"

    # Configure git/ssh keys
    config.vm.provision "shell", privileged: false, inline: <<-SHELL
        mkdir -p ~/.ssh
        chmod 700 ~/.ssh
        ssh-keyscan -Ht rsa github.com >> ~/.ssh/known_hosts
        ssh-keyscan -Ht ecdsa github.com >> ~/.ssh/known_hosts
    SHELL


        config.vm.provision "shell", inline: <<-SHELL
                apt update -y && apt upgrade -y
                apt install -y figlet

                figlet DEPENDENCIES  # ASCII banner

                apt install -y build-essential cmake cscope ctags quilt make git gcc pkgconf libtool libctemplate-dev libjsoncpp-dev libjsoncpp1 libdbus-1-dev \
                libsystemd-dev libyajl-dev libcap-dev go-md2man autoconf automake libseccomp-dev libboost-dev valgrind libcunit1-dev liblog4c-dev libfreetype6-dev \
                libjpeg-dev xorg-dev python3 python3-pip libarchive-dev libcurl4 libcurl4-gnutls-dev libssl-dev libgpgme11-dev libtool-bin libarchive13 bison flex \
                jq docbook2x ninja-build unzip botan libbotan-2-dev libbz2-dev

                #
                # Create build folders, links etc...
                #
                cp /usr/lib/x86_64-linux-gnu/dbus-1.0/include/dbus/dbus-arch-deps.h /usr/include/dbus-1.0/dbus
        SHELL

        config.vm.provision "shell", inline: <<-SHELL
                # We have to compile it as we need a newer version for librnp.
                # ubuntu/jammy is too ancient again.
                figlet CMake  # ASCII banner
                
                git clone -b 2.19.4 https://github.com/randombit/botan.git
                cd botan/
                python3 ./configure.py && make && sudo make install
        SHELL

        config.vm.provision "shell", inline: <<-SHELL
                # We have to compile it as we need a newer version for librnp.
                # We can't update to ubuntu/jammy because we need cgroups in v1 apparently.
                figlet CMake  # ASCII banner
                
                git clone https://github.com/Kitware/CMake.git
                cd CMake/
                git checkout -b v3.22.1 v3.22.1
                ./bootstrap && make -j$(($(nproc)/2)) && sudo make install
        SHELL

        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                figlet UBUS DEPENDENCIES

                sudo apt install -y lua5.1 liblua5.1-0-dev python3.8-venv

                mkdir ~/srcUbus
                cd ~/srcUbus
                git clone https://github.com/json-c/json-c.git # get json-c
                cd json-c
                git checkout 1741bcd
                mkdir build
                cd build
                cmake ..
                make -j$(nproc)
                sudo make install # make json-c available
                LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/vagrant/srcUbus/json-c/build # path for .so for the make installed versions
        SHELL


        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                figlet LIBUBOX

                cd ~/srcUbus
                git clone https://git.openwrt.org/project/libubox.git # get ubox lib
                cd libubox
                git checkout eac92a4d5d82eb31e712157e7eb425af728b2c43
                mkdir build
                cd build
                cmake ..
                make -j$(nproc)
                sudo make install # make ubox lib available for ubus
                LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/vagrant/srcUbus/libubox/build
        SHELL


        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                figlet UBUS # banner

                cd ~/srcUbus
                if [[ -d ubus ]] ; then
                 cd ubus
                 git pull
                else
                 git clone https://git.openwrt.org/project/ubus.git
                 cd ubus
                fi
                git checkout 9913aa61de739e3efe067a2d186021c20bcd65e2
                mkdir build
                cd build
                cmake -D UNIT_TESTING=ON .. # enable unit testing to build ubus-san & ubusd-san
                make -j$(nproc)
                sudo make install
                LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/vagrant/srcUbus/ubus/build

                # permanently update dynamic library path
                export LD_LIBRARY_PATH
                sudo ldconfig

                #add symlinks for ubus-san & ubusd-san
                sudo ln -f -s /home/vagrant/srcUbus/ubus/build/ubus-san /usr/local/bin/ubus-san
                sudo ln -f -s /home/vagrant/srcUbus/ubus/build/ubusd-san /usr/local/sbin/ubusd-san
        SHELL


        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                figlet RBUS  # ASCII banner
                mkdir ~/rbus
                cd ~/rbus
                git clone -b v2.0.5 https://github.com/rdkcentral/rbus.git .
                cmake -DCMAKE_INSTALL_PREFIX=/usr -DBUILD_FOR_DESKTOP=ON -DCMAKE_BUILD_TYPE=Debug
                make -j$(nproc)
                sudo make install -j$(nproc)
        SHELL


        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                figlet meta-lcm
                BRANCH="master"
                CLONE_DIR="meta-lcm"

                cd ~
                if [ ! -d "./$CLONE_DIR" ]; then
                    git clone -b $BRANCH git@gitlab.com:prpl-foundation/prplrdkb/metalayers/meta-lcm.git $CLONE_DIR
                fi
        SHELL

        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                figlet meta-amx
                BRANCH="master"
                CLONE_DIR="meta-amx"
                cd ~
                if [ ! -d "./$CLONE_DIR" ]; then
                     git clone -b $BRANCH https://gitlab.com/prpl-foundation/prplrdkb/metalayers/meta-amx.git $CLONE_DIR
                fi
        SHELL

        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                # Build and install liburiparser
                # liburiparser is a prerequisite for libamxb

                figlet liburiparser

                sudo apt-get install -y liburiparser1
                sudo apt-get install -y liburiparser-dev

        SHELL


        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                # Build and install libevent and related libs
                # libevent is a prerequisite for libamrtx

                figlet libevent

                sudo apt-get install -y libevent-dev
                sudo apt-get install -y libcap-ng-dev

        SHELL


        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                #  Install libamxb_ubus dependency libs and packages

                figlet libamxb_ubus_dep

                sudo apt-get install -y psmisc  # utilities
                sudo apt-get install -y libyajl-dev  # lib for parsing JSON
                sudo apt-get install -y libcmocka-dev  #  provides unit test framework in C
                sudo apt-get install -y gcovr  #  generates summarised code coverage results in unit testing

        SHELL


        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                # Build and install libamxc
                # libamxc has no dependencies
                figlet libamxc
                mkdir -p ~/amx_project/libraries/
                cd ~/amx_project/libraries/
                git clone -b v2.1.0 --depth 1 https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxc.git
                cd ~/amx_project/libraries/libamxc
                make -j$(nproc)
                sudo make install
        SHELL


        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                # Build and install libamxp
                # libamxp depends upon libamxc
                figlet libamxp
                mkdir -p ~/amx_project/libraries/
                cd ~/amx_project/libraries/
                git clone -b v2.2.0 --depth 1 https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxp.git
                cd ~/amx_project/libraries/libamxp
                make -j$(nproc)
                sudo make install
        SHELL


        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                # Build and install libamxd
                # libamxd depends upon libamxc and libamxp
                figlet libamxd
                mkdir -p ~/amx_project/libraries/
                cd ~/amx_project/libraries/
                git clone -b v6.5.5 --depth 1 https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxd.git
                cd ~/amx_project/libraries/libamxd
                make -j$(nproc)
                sudo make install
        SHELL


        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                # Build and install libamxj
                # libamxj depends upon libamxc and libyajl2
                figlet libamxj
                mkdir -p ~/amx_project/libraries/
                cd ~/amx_project/libraries/
                git clone -b  v1.0.3 --depth 1 https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxj.git
                cd ~/amx_project/libraries/libamxj
                make -j$(nproc)
                sudo make install
        SHELL

        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                # Build and install libamxb
                # libamxb depends on:
                # libamxc, libamxp, llibamxd, liburiparser-dev, liburiparser1 (RFC 3986 coimpliant)

                figlet libamxb

                mkdir -p ~/amx_project/libraries/
                cd ~/amx_project/libraries/
                git clone -b v4.11.1 --depth 1 https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxb.git
                cd ~/amx_project/libraries/libamxb
                make -j$(nproc)
                sudo make install

        SHELL

        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                # Build and install libamxs
                # libamxs depends on:
                # libamxc, libamxp, libamxb

                figlet libamxs

                mkdir -p ~/amx_project/libraries/
                cd ~/amx_project/libraries/
                git clone -b v0.6.4 --depth 1 https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxs.git
                cd ~/amx_project/libraries/libamxs
                make -j$(nproc)
                sudo make install

        SHELL

        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                 # Build and install libamxo
                 # libamxo depends upon libamxc, libamxp, libamxd
                 figlet libamxo
                 mkdir -p ~/amx_project/libraries/
                 cd ~/amx_project/libraries/
                 git clone -b v5.0.2 --depth 1 https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxo.git
                 cd ~/amx_project/libraries/libamxo
                 make -j$(nproc)
                 sudo make install
        SHELL

        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                # Build and install libamxt
                # libamxt depends on:
                # libamxc, libamxp

                figlet libamxt

                mkdir -p ~/amx_project/libraries/
                cd ~/amx_project/libraries/
                git clone -b v1.0.0 --depth 1 https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxt.git
                cd ~/amx_project/libraries/libamxt
                make -j$(nproc)
                sudo make install

        SHELL

        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                # Build and install libamxrt
                # libamxrt depends on:
                # libevent libamxc libamxp libamxd libamxo libamxb libamxj libcap-ng

                figlet libamxrt

                mkdir -p ~/amx_project/libraries/
                cd ~/amx_project/libraries/
                git clone -b v0.6.1 --depth 1 https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxrt.git
                cd ~/amx_project/libraries/libamxrt
                make -j$(nproc)
                sudo make install

        SHELL

        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                # Build and install amxrt
                # libamxrt depends on:
                # libamxc, libamxp, libamxb, libamxd, libamxo, libevent-dev

                figlet amxrt
                mkdir -p ~/amx_project/applications
                cd ~/amx_project/applications
                git clone -b v2.2.0 --depth 1 https://gitlab.com/prpl-foundation/components/ambiorix/applications/amxrt.git
                cd ~/amx_project/applications/amxrt
                make -j$(nproc)
                sudo -E make install

        SHELL


        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                # Build and install libamxb_ubus
                # libamxb_ubus depends on:
                # libamxc, libamxp, llibamxb, libubox, and ubus

                figlet libamxb_ubus

                mkdir -p ~/amx_project/modules/amxb_backends/
                cd ~/amx_project/modules/amxb_backends/
                git clone -b v3.4.0 --depth 1 https://gitlab.com/prpl-foundation/components/ambiorix/modules/amxb_backends/amxb_ubus.git
                cd ~/amx_project/modules/amxb_backends/amxb_ubus
                make -j$(nproc)
                sudo make install

        SHELL


        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                # Build and install libamxm
                # Depends on libamxc
                figlet libamxm
                mkdir -p ~/amx_project/libraries/
                cd ~/amx_project/libraries
                git clone -b v0.1.0 --depth 1 https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxm.git
                cd libamxm
                make -j$(nproc)
                sudo make install
        SHELL

        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                # Libamxtui is a library that provides terminal user interface widgets based on ncurses.
                 # Depends libamxc libamxt libamxp ncurses
                 figlet Libamxtui

                 sudo apt-get install libncurses5-dev libncursesw5-dev

                 mkdir -p ~/amx_project/amxlab/tui/libraries/
                 cd ~/amx_project/amxlab/tui/libraries/
                 git clone -b v0.1.7 --depth 1 https://gitlab.com/prpl-foundation/components/ambiorix/amxlab/tui/libraries/libamxtui.git
                 cd ~/amx_project/amxlab/tui/libraries/libamxtui
                 make -j$(nproc)
                 sudo make install
        SHELL

        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                # dmtui is an application that provides a GUI interface for the datamodels.
                # Depends on libamxc libamxp libamxt libamxd libamxo libamxb libamxtui
                figlet dmtui

                mkdir -p ~/amx_project/amxlab/tui/applications/
                cd ~/amx_project/amxlab/tui/applications/
                git clone -b v0.3.3 https://gitlab.com/prpl-foundation/components/ambiorix/amxlab/tui/applications/dmtui.git
                cd ~/amx_project/amxlab/tui/applications/dmtui
                patch -p1 < ~/meta-amx/recipes-applications/dmtui/001_select_objects_to_be_shown.patch
                make -j$(nproc)
                sudo make install
        SHELL


        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                # Build and install the ambiorix rbus backend
                figlet amxb_rbus

                mkdir -p ~/amx_project/modules/amxb_backends/
                cd ~/amx_project/modules/amxb_backends/
                git clone -b 1.3.4 https://gitlab.com/prpl-foundation/components/ambiorix/modules/amxb_backends/amxb_rbus.git
                cd ~/amx_project/modules/amxb_backends/amxb_rbus

                make -j$(nproc)
                sudo make install
        SHELL

        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                 # Build and install libsahtrace
                 # No dependencies
                 figlet libsahtrace

                 mkdir -p ~/amx_project/libraries/
                 cd ~/amx_project/libraries/
                 git clone -b v1.15.0 --depth 1 https://gitlab.com/prpl-foundation/components/core/libraries/libsahtrace.git
                 cd libsahtrace
                 make -j$(nproc)
                 sudo make install
         SHELL


        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                 # Build and install mod-sahtrace

                 # requires libamxc, libamxo, libamxd, libsahtrace
                 figlet 'mod-sahtrace'

                 cd ~
                 mkdir -p ~/amx_project/modules/
                 cd ~/amx_project/modules/

                 git clone -b v1.2.2 --depth 1 https://gitlab.com/soft.at.home/ambiorix/modules/mod-sahtrace.git
                 cd ~/amx_project/modules/mod-sahtrace
                 make -j$(nproc)
                 sudo make install

         SHELL

        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                # Build and install mod-dmproxy

                # requires libamxc, libamxp, libamxo, libamxd, libsahtrace
                figlet 'mod-dmproxy'

                mkdir -p ~/amx_project/modules/
                cd ~/amx_project/modules/

                git clone -b v1.4.1 --depth 1 https://gitlab.com/prpl-foundation/components/core/modules/mod-dmproxy.git
                cd ~/amx_project/modules/mod-dmproxy
                make -j$(nproc)
                sudo make install

        SHELL

         config.vm.provision "shell", privileged: false, inline: <<-SHELL
                 # Lib_amxut is a library providing utilities for writing unittests for Ambiorix components
                 # libamxc, libamxp, libamxb, libamxd, libamxo, libsahtrace
                 figlet libamxut

                 mkdir -p ~/amx_project/libraries/
                 cd ~/amx_project/libraries/
                 git clone -b v1.10.1 https://gitlab.com/prpl-foundation/components/ambiorix/tools/libraries/libamxut.git
                 cd ~/amx_project/libraries/libamxut
                 make -j$(nproc)
                 sudo make install
         SHELL


        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                # Build and install libocispec
                # requires yajl so must be defined after that in Makefile
                figlet libocispec

                cd ~
                wget -O image-spec-v1.0.1.zip https://github.com/opencontainers/image-spec/archive/refs/tags/v1.0.1.zip
                wget -O runtime-spec-v1.0.2.zip https://github.com/opencontainers/runtime-spec/archive/refs/tags/v1.0.2.zip

                unzip image-spec-v1.0.1.zip
                unzip runtime-spec-v1.0.2.zip

                curl "https://gitlab.com/prpl-foundation/prplos/feeds/feed-prpl/-/raw/prplos/lcm/libs/libocispec/patches/00-shared_object.patch?ref_type=heads&inline=false" -o 00-shared_object.patch

                git clone https://github.com/containers/libocispec.git
                cd ~/libocispec
                git checkout 46b870958a39547026c5dc9a044951e232ecbd9c

                cd ~
                cp -a image-spec-1.0.1/* ~/libocispec/image-spec
                cp -a runtime-spec-1.0.2/* ~/libocispec/runtime-spec

                cd ~/libocispec
                patch -p1 < ~/00-shared_object.patch

                ./autogen.sh
                ./configure --enable-shared --libdir=/usr/lib --includedir=/usr/include --disable-dependency-tracking --disable-static
                make -j$(nproc)
                #the install target for this library does nothing, so manually install the files instead
                sudo mkdir -p /usr/include/libocispec/
                sudo cp src/*.h /usr/include/libocispec/
                sudo cp .libs/libocispec.so /usr/lib/x86_64-linux-gnu/libocispec.so
        SHELL

        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                # Build and install liblcm
                # Depends on libamxc
                figlet liblcm
                mkdir -p ~/PrplLCM/lib
                cd ~/PrplLCM/lib
                git clone -b v0.2.5 --depth 1 https://gitlab.com/prpl-foundation/lcm/libraries/liblcm.git
                cd liblcm
                make -j$(nproc)
                sudo make install

        SHELL

        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                # Build and install librlyeh
                # requires lib_amxc lib_amxj yajl libocispec, so must be defined after those in Makefile
                figlet librlyeh

                mkdir -p ~/PrplLCM/lib
                cd ~/PrplLCM/lib
                git clone -b v1.2.0 --depth 1 https://gitlab.com/prpl-foundation/lcm/libraries/librlyeh.git
                cd librlyeh
                make -j $(nproc)
                sudo -E make install

        SHELL


        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                #
                figlet Greeter

                mkdir -p ~/amx_project/applications/DemoGreeter
                cd ~/amx_project/applications/DemoGreeter
                git clone -b v0.4.3  --depth 1 https://gitlab.com/prpl-foundation/components/ambiorix/examples/datamodel/greeter_plugin.git
                cd ~/amx_project/applications/DemoGreeter/greeter_plugin
                make -j$(nproc)
                sudo make install

        SHELL


        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                #Build and install LXC
                figlet LXC
                cd ~
                git clone -b lxc-5.0.2 --depth 1 https://github.com/lxc/lxc.git # get version 5.0.2
                cd lxc
                sudo python3 -m pip install meson
                sudo /usr/local/bin/meson setup -Dprefix=/usr build
                sudo /usr/local/bin/meson compile -C build
                sudo make install
        SHELL

# Decided to use LXC version 4.0.10 because RDK didn't updated to LXC 5.x. Prpl LCM does not have any depency on LXC 5.x.
#        config.vm.provision "shell", privileged: false, inline: <<-SHELL
#                #Build and install LXC
#                figlet LXC
#                cd ~
#                git clone -b lxc-4.0.10 --depth 1 https://github.com/lxc/lxc.git # get version 4.0.10
#                cd lxc
#                patch -p1 < ~/patches/lxc/0001-Parse-quotes-at-lxc-init-cmd.patch
#                ./autogen.sh
#                ./configure
#                make -j$(nproc)
#                sudo make install
#                export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/home/vagrant/lxc/src/lxc/.libs
#                sudo ldconfig # possibly unnecessary but I needed it to get it working
#        SHELL


        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                # Build and install libcthulhu
                # Depends on libamxc
                figlet libcthulhu
                mkdir -p ~/PrplLCM/lib
                cd ~/PrplLCM/lib
                git clone -b v0.27.0 https://gitlab.com/prpl-foundation/lcm/libraries/libcthulhu.git
                cd libcthulhu

                make -j$(nproc)
                sudo make install
        SHELL

        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                # Build and install cthulhu-lxc plugin
                # depends on libamxc libamxp libamxd libamxo libamxm libcthulhu libsahtrace and lxc
                figlet cthulhu-lxc
                mkdir -p ~/PrplLCM/modules
                cd ~/PrplLCM/modules
                git clone -b v0.10.0 https://gitlab.com/prpl-foundation/lcm/modules/cthulhu-lxc.git
                cd cthulhu-lxc
                patch -p1 < ~/meta-lcm/recipes-mods/cthulhu-lxc/cthulhu-lxc/001-cthulhu-lxc-disable-autodev.patch
                make -j$(nproc)
                sudo make install
        SHELL

        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                # Build and install timingila
                # requires libamxc libamxp libamxd libamxo libamxm libyajl-dev sah-lib-sahtrace-dev libocispec , so must be defined after those in Makefile
                figlet 'timingila'

                mkdir -p ~/PrplLCM/applications
                cd ~/PrplLCM/applications
                git clone -b v2.3.1 https://gitlab.com/prpl-foundation/lcm/applications/timingila.git
                cd timingila
                patch -p1 < /home/vagrant/meta-lcm/recipes-applications/timingila/timingila/001-timingila-device-softwaremodules.patch
                if ENV['BUNDLE_SUPPORT'] == '1'
                    make DEFINE_ALL_BOOLEANS=y CONFIG_SAH_SERVICES_TIMINGILA_ADAPTER_PACKAGER=/usr/lib/timingila-celephais/timingila-celephais.so -j$(nproc)
                else
                    make DEFINE_ALL_BOOLEANS=y CONFIG_SAH_SERVICES_TIMINGILA_ADAPTER_PACKAGER=/usr/lib/timingila-rlyeh/timingila-rlyeh.so -j$(nproc)
                end
                sudo make install

        SHELL

        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                # Build and install the timingila-rlyeh adapter library
                # requires libamxc libamxp libamxb libamxd libamxm librlyeh , so must be defined after those in Makefile
                figlet 'timingila-rlyeh'

                mkdir -p ~/PrplLCM/modules
                cd ~/PrplLCM/modules
                git clone -b v1.0.8 --depth 1 https://gitlab.com/prpl-foundation/lcm/modules/timingila-rlyeh.git
                cd timingila-rlyeh
                make -j$(nproc)
                sudo make install

        SHELL


        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                figlet timingila-cthulhu

                mkdir -p ~/PrplLCM/modules
                cd ~/PrplLCM/modules
                git clone -b v2.2.0 --depth 1 https://gitlab.com/prpl-foundation/lcm/modules/timingila-cthulhu.git
                cd timingila-cthulhu
                make -j$(nproc)
                sudo make install
        SHELL


        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                # Build and install rlyeh
                # Depends on libamxc libamxp libamxd libamxo libyajl-dev sah-lib-sahtrace-dev librlyeh
                figlet rlyeh

                mkdir -p ~/PrplLCM/applications
                cd ~/PrplLCM/applications
                git clone -b v2.4.0 --depth 1 https://gitlab.com/prpl-foundation/lcm/applications/rlyeh.git
                cd rlyeh
                make -j $(nproc)
                sudo make install

        SHELL


        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                # Build and install cthulhu
                # Depends on libamxc,libamxp,libamxd,libamxo,libamxm,libsahtrace,libyajl-dev,libarchive-dev
                figlet cthulhu

                sudo apt-get install -y pkg-config libnl-3-dev libnl-route-3-dev
                mkdir -p ~/PrplLCM/applications
                cd ~/PrplLCM/applications
                git clone -b v3.9.0 https://gitlab.com/prpl-foundation/lcm/applications/cthulhu.git
                cd cthulhu
                patch -p1 < ~/meta-lcm/recipes-applications/cthulhu/cthulhu/001-bundle-support-fixes.patch
                patch -p1 < ~/meta-lcm/recipes-applications/cthulhu/cthulhu/002-enable-bundle-support-odl-upstream.patch
                
                make -j$(nproc)
                sudo make install
        SHELL

        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                figlet "Cthulhu plugins are not installed by default"
                # Add dhcp client support to Cthulhu sandboxes
                # Depends on libamxc,libamxp,libamxd,libamxo,libamxb,libamxm,libsahtrace,libcthulhu,libocispec,liblcm
                figlet cthulhu-dhcpc plugin

                mkdir -p ~/PrplLCM/plugins/cthulhu
                cd ~/PrplLCM/plugins/cthulhu
                git clone -b v1.3.0 https://gitlab.com/prpl-foundation/lcm/plugins/cthulhu/cthulhu-dhcpc.git
                #cd cthulhu-dhcpc
                #make -j$(nproc)
                #sudo make install
        SHELL

        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                figlet "Cthulhu plugins are not installed by default"
                # Add networking support to Cthulhu containers
                # Depends on libamxc,libamxp,libamxd,libamxo,libamxb,libamxm,libsahtrace,libcthulhu,libocispec,liblcm
                figlet cthulhu-networking plugin

                mkdir -p ~/PrplLCM/plugins/cthulhu
                cd ~/PrplLCM/plugins/cthulhu
                git clone -b v0.2.4 https://gitlab.com/prpl-foundation/lcm/plugins/cthulhu/cthulhu-networking.git
                #cd cthulhu-networking
                #make -j$(nproc)
                #sudo make install
        SHELL

        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                figlet "Cthulhu plugins are not installed by default"
                # Add onboarding module - Pre-Embedded containers
                # Depends on libamxc,libamxp,libamxd,libamxo,libamxb,libamxm,libsahtrace,libcthulhu,libocispec,liblcm
                figlet cthulhu-onboarding plugin

                mkdir -p ~/PrplLCM/plugins/cthulhu
                cd ~/PrplLCM/plugins/cthulhu
                git clone -b v1.0.0 https://gitlab.com/prpl-foundation/lcm/plugins/cthulhu/cthulhu-onboarding.git
                #cd cthulhu-onboarding
                #make -j$(nproc)
                #sudo make install
        SHELL

        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                # Add libsexpp for S expressions library
                # needed by librnp
                figlet libsexpp

                mkdir -p ~/PrplLCM/lib
                cd ~/PrplLCM/lib
                git clone -b v0.8.8 https://github.com/rnpgp/sexpp.git
                cd sexpp/
                mkdir build
                cd build
                cmake -DBUILD_SHARED_LIBS=ON -DWITH_SEXP_TESTS=OFF -DWITH_SEXP_CLI=OFF ../

                make -j$(nproc)
                sudo make install
        SHELL

        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                # Add librnp authentication library
                # Needed by rlyeh image auth

                figlet librnp

                mkdir -p ~/PrplLCM/lib
                cd ~/PrplLCM/lib
                git clone -b v0.17.1 --recurse-submodules --shallow-submodules https://github.com/rnpgp/rnp.git
                cd rnp/
                mkdir build
                cd build
                cmake -DBUILD_SHARED_LIBS=ON -DBUILD_TESTING=OFF -DSYSTEM_LIBSEXPP=ON ../

                make -j$(nproc)
                sudo make install
        SHELL

        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                # Add rlyeh authentication module
                # Depends on libamxc,libamxp,libamxd,libamxo,libamxb,libamxm,libsahtrace,libcthulhu,libocispec,liblcm
                figlet rlyeh image authentication plugin

                mkdir -p ~/PrplLCM/plugins/rlyeh/rlyeh-auth
                cd ~/PrplLCM/plugins/rlyeh/rlyeh-auth
                git clone -b v0.1.0 https://gitlab.com/prpl-foundation/lcm/plugins/rlyeh/image-authentication.git .

                make -j$(nproc)
                # both on prplOS as well as here the installation fails without this directory.
                mkdir keys/
                cp test/common/keys/lcmtests-pub.asc keys/
                sudo make install
        SHELL


        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                # Build and install libamxa
                # Depends on libamxc, libamxj, libamxd, libamxb
                figlet libamxa
                mkdir -p ~/amx_project/libraries/
                cd ~/amx_project/libraries
                git clone -b v0.11.0 --depth 1 https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxa.git
                cd libamxa
                make -j$(nproc)
                sudo make install
        SHELL

        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                # Build and install amx-cli
                # amx-cli depends on:
                # libamxo, libamxc, libamxd, libamxj, libamxt, libamxm

                figlet amx-cli

                mkdir -p ~/amx_project/applications
                cd ~/amx_project/applications
                git clone -b v0.5.1 --depth 1 https://gitlab.com/prpl-foundation/components/ambiorix/applications/amx-cli.git
                cd amx-cli
                make -j$(nproc)
                sudo -E make install

        SHELL

        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                # Build and install mod-ba-cli
                # mod-ba-cli depends on:
                # libamxc, libamxt, libamxm, libamxp, libamxd, libamxb, libamxo, libamxa

                figlet mod-ba-cli

                mkdir -p ~/amx_project/modules/amx_cli
                cd ~/amx_project/modules/amx_cli
                git clone -b v0.9.0 --depth 1 https://gitlab.com/prpl-foundation/components/ambiorix/modules/amx_cli/mod-ba-cli.git
                cd mod-ba-cli
                patch -p1 < ~/meta-amx/recipes-modules/mod-ba-cli/mod-ba-cli/001-mod-ba-cli-autoconnect.patch
                patch -p1 < ~/meta-amx/recipes-modules/mod-ba-cli/mod-ba-cli/002-mod-ba-cli-no-logo.patch
                patch -p1 < ~/meta-amx/recipes-modules/mod-ba-cli/mod-ba-cli/003-mod-ba-cli-configurable-timeout.patch
                patch -p1 < ~/meta-amx/recipes-modules/mod-ba-cli/mod-ba-cli/004-mod-ba-cli-invalid-path-check.patch
                patch -p1 < ~/meta-amx/recipes-modules/mod-ba-cli/mod-ba-cli/005-mod-ba-cli-rbus-variables.patch
                make -j$(nproc)
                sudo -E make install
        SHELL


        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                figlet prpl-lcm-links

                # Creates symbolic links as some prpl lcm applications expect files
                # to be in a different location as to where they are installed

                sudo ln -f -s /usr/lib/x86_64-linux-gnu/amx/cthulhu/cthulhu.so /etc/amx/cthulhu/cthulhu.so
                sudo ln -f -s /usr/lib/x86_64-linux-gnu/amx/rlyeh/rlyeh.so /etc/amx/rlyeh/rlyeh.so
                sudo ln -f -s /usr/lib/x86_64-linux-gnu/amx/timingila/timingila.so /etc/amx/timingila/timingila.so

                sudo ln -f -s /usr/lib/x86_64-linux-gnu/cthulhu-lxc /usr/lib/cthulhu-lxc
                sudo ln -f -s /usr/lib/x86_64-linux-gnu/cthulhu-crun /usr/lib/cthulhu-crun
                sudo ln -f -s /usr/lib/x86_64-linux-gnu/timingila-cthulhu /usr/lib/timingila-cthulhu
                sudo ln -f -s /usr/lib/x86_64-linux-gnu/timingila-rlyeh /usr/lib/timingila-rlyeh
                sudo ln -f -s /usr/lib/x86_64-linux-gnu/timingila-celephais /usr/lib/timingila-celephais
        SHELL

#########################################################
################### USP Dependencies ####################
#########################################################

        # config.vm.provision "shell", inline: <<-SHELL
        #         figlet USP DEPENDENCIES  # ASCII banner

        #         apt install -y libssl-dev libcurl4-openssl-dev libsqlite3-dev libz-dev autoconf automake libtool  pkg-config net-tools
        #         apt-add-repository -y ppa:mosquitto-dev/mosquitto-ppa
        #         apt-get update
        #         apt-get install -y libmosquitto-dev libwebsockets-dev
        # SHELL

#        config.vm.provision "shell", privileged: false, inline: <<-SHELL
#                figlet USP-setup  # ASCII banner
#                # Get and setup USP
#                mkdir -p ~/srcUsp
#                cd ~/srcUsp
#                git clone -b v5.0.0-master --depth 1 https://github.com/BroadbandForum/obuspa.git
#                cd ./obuspa
#                patch -p1 < /home/vagrant/patches/obuspa/vendor.c.patch # Patch vendor integration to pull in LCM data model
#                patch -p1 < /home/vagrant/patches/obuspa/vendor.am.patch # Patch vendor integration to pull in LCM data model
#        SHELL


#       config.vm.provision "shell", privileged: false, inline: <<-SHELL
#                figlet USP-prpl  # ASCII banner
#                cd ~/srcUsp/obuspa
#                cp /home/vagrant/meta-lcm/recipes-usp/usp/files/lcm_rbus_datamodel.c src/vendor/lcm_rbus_datamodel.c # copy LCM code
#                patch -p1 src/vendor/lcm_rbus_datamodel.c /home/vagrant/meta-lcm/recipes-usp/usp/files/lcm_rbus_datamodel_timingila_no_device.patch # patch datamodel for prpl usage(timingila does not use "Device.")
#        SHELL


#        config.vm.provision "shell", privileged: false, inline: <<-SHELL
#                figlet USP  # ASCII banner
#                cd ~/srcUsp/obuspa
#                autoreconf --force --install
#                ./configure
#                make
#                sudo make install
#        SHELL
#########################################################
#########################################################

#########################################################
####### Add support for crun and oci bundles ############
#########################################################

         config.vm.provision "shell", privileged: false, inline: <<-SHELL
                 figlet CRUN  # ASCII banner
                 mkdir ~/crun
                 cd ~/crun
                 git clone -b 1.6 --depth 1 https://github.com/containers/crun.git .

                 sudo mkdir -p /usr/lib/plugins/dobby
                 sudo mkdir -p /opt/persistent/rdk
                 sudo mkdir -p /var/volatile/rdk/dobby/bundles
                 sudo chmod 755 /var/volatile/rdk/dobby/bundles

                 # This patch causes ./configure to append "dirty" to the end of the version number
                 # as the source code no longer matches what's in the git repository
                 patch -p1 < ~/meta-lcm/recipes-containers/crun/files/0001-Add-libcrun-get-version.patch

                 ./autogen.sh
                 ./configure --enable-shared
                 sudo make install -j$(nproc)
                 sudo ln -f -s /usr/bin/crun /usr/bin/crun

                 sudo mkdir -p /usr/include/crun
                 sudo cp ./config.h /usr/include/crun
                 sudo cp ./src/libcrun/container.h /usr/include/crun
                 sudo cp ./src/libcrun/error.h /usr/include/crun
                 sudo cp ./src/libcrun/status.h /usr/include/crun

                 sudo mkdir -p /usr/include/crun/libocispec
                 sudo cp ./libocispec/src/*.h /usr/include/crun/libocispec

                 sudo mkdir -p /usr/lib
                 sudo cp ./libocispec/.libs/libocispec.* /usr/lib

                 # Refreshes linker to make sure the new crun and libocispec headers can be found
                 sudo ldconfig
                 # Fix crun search paths
                 sudo mkdir -p /run/rdk
                 sudo ln -sf /var/run/crun/ /run/rdk/crun
         SHELL
#########################################################
#########################################################

        config.vm.provision "shell", privileged: false, inline: <<-SHELL
            figlet cthulhu-crun
            # Build and install cthulhu-crun plugin
            mkdir -p ~/PrplLCM
            cd ~/PrplLCM
            
            git clone -b master https://gitlab.com/prpl-foundation/lcm/modules/cthulhu-crun.git
            
            cd ~/PrplLCM/cthulhu-crun
            
            make -j$(nproc)
            sudo make install
        SHELL


        config.vm.provision "shell", privileged: false, inline: <<-SHELL
            figlet libcelephais
            #build and install celephais bundle packager support library
            BRANCH="v1.0.0"
            CLONE_DIR="libcelephais"

            cd ~
            if [ ! -d "./$CLONE_DIR" ]; then
                git clone -b $BRANCH https://gitlab.com/prpl-foundation/lcm/libraries/libcelephais.git $CLONE_DIR
            fi

            cd ~/$CLONE_DIR
            git am ~/meta-lcm/recipes-lib/libcelephais/files/0001-LCM-806-Decouple-LIBDIR-and-fix-Makefile.patch
            make -j$(nproc)
            sudo make install
        SHELL


        config.vm.provision "shell", privileged: false, inline: <<-SHELL
            figlet timingila-celephais
            # Builds and install the Timingila-Celephais adapter
            BRANCH="master"
            CLONE_DIR="timingila-celephais"

            cd ~
            if [ ! -d "./$CLONE_DIR" ]; then
                git clone -b $BRANCH https://gitlab.com/prpl-foundation/lcm/modules/timingila-celephais.git $CLONE_DIR
            fi

            mkdir -p ~/PrplLCM
            ln -f -s ~/$CLONE_DIR ~/PrplLCM/timingila-celephais

            cd ~/PrplLCM/timingila-celephais
            make -j$(nproc)
            sudo make install
        SHELL


        config.vm.provision "shell", privileged: false, inline: <<-SHELL
            figlet celephais
            #build and install celephais bundle packager
            #depends on libamxc libamxp libamxd libamxo libyajl-dev sah-lib-sahtrace-dev
            BRANCH="master"
            CLONE_DIR="celephais"

            cd ~
            if [ ! -d "./$CLONE_DIR" ]; then
                git clone -b master https://gitlab.com/prpl-foundation/lcm/applications/celephais.git $CLONE_DIR
            fi

            cd ~/$CLONE_DIR
            mkdir build
            cd build
            cmake ../
            make -j$(nproc)
            sudo make install
        SHELL

        if ENV['BUNDLE_SUPPORT'] == '1'
            config.vm.provision "shell", privileged: false, inline: <<-SHELL

                figlet celephais-setup

                #build timingila with USE_CELEPHAIS=1
                cd ~/PrplLCM/applications

                AMX_CTHULHU_CONFIG_ODL=/etc/amx/cthulhu/cthulhu_config.odl
                CTHULHU_ODL=/etc/config/cthulhu/odl/cthulhu.odl
                USE_CRUN=true

                if [ -f "$AMX_CTHULHU_CONFIG_ODL" ]; then
                    if [[ $USE_CRUN == true ]]; then
                        sudo sed -i 's#cthulhu-lxc#cthulhu-crun#g' $AMX_CTHULHU_CONFIG_ODL
                    else
                        sudo sed -i 's#cthulhu-crun#cthulhu-lxc#g' $AMX_CTHULHU_CONFIG_ODL
                    fi
                fi

                if [ -f "$CTHULHU_ODL" ]; then
                    if [[ $USE_CRUN == true ]]; then
                        sudo sed -i 's#cthulhu-lxc#cthulhu-crun#g' $CTHULHU_ODL
                    else
                        sudo sed -i 's#cthulhu-crun#cthulhu-lxc#g' $CTHULHU_ODL
                    fi
                fi

                #add "parameter 'UseBundles' = true;" to Cthulhu.Config (generates a awk warning but has to be double escaped due to parsing done by vagrantfile)
                sudo awk -i inplace '/BundleLocation/{print "            parameter '\\\''UseBundles'\\\'' = true;"}1' /etc/amx/cthulhu/cthulhu_config.odl

                chmod 755 ~/prpl-lcm-startup-celephais.sh
            SHELL
        end

        config.vm.provision "shell", privileged: false, inline: <<-SHELL
                figlet DONE
                echo "**************************************************************************"
                echo "***                     Vagrant provision complete!                    ***"
                echo "***               Run 'vagrant ssh' to SSH to your new VM              ***"
                echo "**************************************************************************"
        SHELL
end
